//Elissar 2134398

package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    private final double DELTA = 1e-15;
    private Vector3d v = new Vector3d(1, 2, 3);

    @Test
    public void testGetMethod() {
        assertEquals(1, v.getX(), DELTA);

        assertEquals(2, v.getY(), DELTA);

        assertEquals(3, v.getZ(), DELTA);
    }

    @Test
    public void testMagnitudeMethod() {
        assertEquals(Math.sqrt(14), v.magnitude(), DELTA);
    }

    @Test
    public void testDotProductMethod() {
        Vector3d v2 = new Vector3d(10, 10, 10);

        assertEquals(60, v.dotProduct(v2), DELTA);
    }

    @Test
    public void testAddMethod() {
        Vector3d v2 = new Vector3d(1, 1, 1);
        Vector3d v3 = v.add(v2);
        Vector3d vExpected = new Vector3d(2, 3, 4);

        assertEquals(vExpected.getX(), v3.getX(), DELTA);

        assertEquals(vExpected.getY(), v3.getY(), DELTA);

        assertEquals(vExpected.getZ(), v3.getZ(), DELTA);
    }
}
