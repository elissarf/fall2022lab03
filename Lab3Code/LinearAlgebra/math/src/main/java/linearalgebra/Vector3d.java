//Elissar 2134398
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public double dotProduct(Vector3d vector2) {
        return (vector2.getX() * this.x) + (vector2.getY() * this.y) + (vector2.getZ() * this.z);
    }

    public Vector3d add(Vector3d vector2) {
        double newX = vector2.getX() + this.x;
        double newY = vector2.getY() + this.y;
        double newZ = vector2.getZ() + this.z;

        return new Vector3d(newX, newY, newZ);
    }
}
